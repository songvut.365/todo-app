import React from 'react'
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';
import Button from 'react-bootstrap/Button';

function Home() {
    return (
        <Container>
            <Row>
                <Col lg={8} className="mb-3">
                    <Card>
                    <Card.Body>
                        <h3>What are you want to do ?</h3>
                        <InputGroup className="my-3">
                        <FormControl 
                            placeholder="Do something . . ."
                            aria-label="Do something . . ."
                        />
                        <Button variant="outline-secondary">Add</Button>
                        </InputGroup>

                        <p>Nothing to do</p>
                    </Card.Body>
                    </Card>
                </Col>

                <Col lg={4} className="mb-3">
                    <Card>
                    <Card.Body>
                        <h5>Right Box</h5>
                    </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    )
}

export default Home
