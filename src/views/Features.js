import React from 'react'

import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Card from 'react-bootstrap/Card'

function Features() {
  return (
        <Container>
            <Row>
                <Col>
                    <Card>
                        <Card.Body>
                            <h3>Features</h3>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    )
}

export default Features
