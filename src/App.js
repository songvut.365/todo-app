import './App.css';
import {Route, Switch, BrowserRouter as Router} from 'react-router-dom';

import Appbar from './components/Appbar';
import Home from './views/Home';
import Features from './views/Features';
import About from './views/About'

function App() {
  return (
    <div>
      <Router>
        <Appbar/>
        <Switch>
          <Route exact path="/" component={Home}></Route>
          <Route exact path="/features" component={Features}></Route>
          <Route exact path="/about" component={About}></Route>
        </Switch>
      </Router>
    </div>
  );
    

}

export default App;